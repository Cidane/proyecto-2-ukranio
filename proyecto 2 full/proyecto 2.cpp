#include "gfx.h"
#include <unistd.h>
#include <iostream>
#include <vector>
#include <stdlib.h>       /* srand, rand */
#include <time.h>         /* time */
#include <cmath>          /* sin, cos */

#include "asteroide.cpp"




using namespace std;


int main() {
  int n;

  srand (time(NULL));     // initialize random seed.

  cout << "Ingrese el numero de asteroides: ";
  cin >> n;

  gfx_open(X_SIZE, Y_SIZE, "Asteroides");
  gfx_color(0,200,100);

  generateAsteroids(n);

  while (true) {
    gfx_clear();

    rotateAsteroids();
    moveAsteroids();
    drawAsteroids();    

    gfx_flush();
    usleep(TIME_SLEEP);
  }

  return 0;
} // */
