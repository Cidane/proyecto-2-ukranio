#include "gfx.h"
#include <unistd.h>
#include <iostream>
#include <vector>
#include <stdlib.h>       /* srand, rand */
#include <time.h>         /* time */
#include <cmath>  
using namespace std;

class Point{
 private:
    double x;
    double y;
  public:
    Point();
    Point(double x, double y);
    void setX(double x);
    void setY(double y);
    double getX();
    double getY();
};

Point::Point() {
  setX(0);
  setY(0);
}

Point::Point(double x_, double y_) {
  x = x_;
  y = y_;
}

void Point::setX(double x_) {
  x = x_;
}

void Point::setY(double y_) {
  y = y_;
}

double Point::getX() {
  return x;
}

double Point::getY() {
  return y;
}
